import CartParser from './CartParser';
import path from 'path';
import uuid from 'uuid/v4';
import { readFileSync } from 'fs';
jest.mock('uuid/v4');

let parser, parse, validate, parseLine, calcTotal, createError,
	validFormatCsvFilePath, invalidFormatCsvFilePath, resultFilePath;

beforeEach(() => {
	parser = new CartParser();
	parse = parser.parse.bind(parser);
	validate = parser.validate.bind(parser);
	parseLine = parser.parseLine.bind(parser);
	calcTotal = parser.calcTotal.bind(parser);
	createError = parser.createError.bind(parser);
	invalidFormatCsvFilePath = path.resolve(__dirname,'../samples/invalid.format.csv');
	validFormatCsvFilePath = path.resolve(__dirname,'../samples/cart.csv');
	resultFilePath = path.resolve(__dirname,'../samples/cart.json');
	uuid.mockImplementation(() => 'testid');
});

describe('CartParser - unit tests', () => {
	it('should throw exception when csv file structure is incorrect for parsing', () => {
		expect(() => { parse(invalidFormatCsvFilePath); }).toThrow('Validation failed!');
	});

	it('shouldn`t throw exception when csv file structure is correct for parsing', () => {
		expect(() => { parse(validFormatCsvFilePath); }).not.toThrow('Validation failed!');
	});

	it('should consist error about unexpected header name', () => {
		const invalidFormatCsvFileContent = readFileSync(invalidFormatCsvFilePath, 'utf-8', 'r');

		const expectedError = { 
			type: 'header',
			row: 0,
			column: 1,
			message: 'Expected header to be named "Price" but received Amount.' 
		};

		expect(validate(invalidFormatCsvFileContent)).toContainEqual(expectedError);
	});

	it('shouldn`t consist error about unexpected header name', () => {
		const validFormatCsvFileContent = readFileSync(validFormatCsvFilePath, 'utf-8', 'r');

		const expectedError = { 
			type: 'header',
			row: 0,
			column: 1,
			message: 'Expected header to be named "Price" but received Amount.' 
		};

		expect(validate(validFormatCsvFileContent)).not.toContainEqual(expectedError);
	});

	it('should consist error about unexpected number of cells', () => {
		const invalidFormatCsvFileContent = readFileSync(invalidFormatCsvFilePath, 'utf-8', 'r');

		const expectedError = { 
			type: 'row',
			row: 1,
			column: -1,
			message: 'Expected row to have 3 cells but received 2.'
		};

		expect(validate(invalidFormatCsvFileContent)).toContainEqual(expectedError);
	});

	it('should consist error about unexpected empty string', () => {
		const invalidFormatCsvFileContent = readFileSync(invalidFormatCsvFilePath, 'utf-8', 'r');

		const expectedError = { 
			type: 'cell',
			row: 5,
			column: 0,
			message: 'Expected cell to be a nonempty string but received "".' 
		}

		expect(validate(invalidFormatCsvFileContent)).toContainEqual(expectedError);
	});

	it('should consist error about unexpected negative number', () => {
		const invalidFormatCsvFileContent = readFileSync(invalidFormatCsvFilePath, 'utf-8', 'r');

		const expectedError = { 
			type: 'cell',
        	row: 4,
        	column: 2,
			message: 'Expected cell to be a positive number but received "-10".' 
		}

		expect(validate(invalidFormatCsvFileContent)).toContainEqual(expectedError);
	});

	it('should return an correct object with a parsed csv line', () => {
		const csvLine = 'Mollis consequat,9.00,2';

		const expectedLineObject = {
            id: uuid(),
            name: 'Mollis consequat',
            price: 9,
            quantity: 2
		}

		expect(parseLine(csvLine)).toEqual(expectedLineObject);
	});

	it('should return a correct total price with a rounding precision of 2', () => {
		const cartItems = [
			{
				"id": "3e6def17-5e87-4f27-b6b8-ae78948523a9",
				"name": "Mollis consequat",
				"price": 9,
				"quantity": 2
			},
			{
				"id": "90cd22aa-8bcf-4510-a18d-ec14656d1f6a",
				"name": "Tvoluptatem",
				"price": 10.32,
				"quantity": 1
			},
			{
				"id": "33c14844-8cae-4acd-91ed-6209a6c0bc31",
				"name": "Scelerisque lacinia",
				"price": 18.9,
				"quantity": 1
			},
			{
				"id": "f089a251-a563-46ef-b27b-5c9f6dd0afd3",
				"name": "Consectetur adipiscing",
				"price": 28.72,
				"quantity": 10
			},
			{
				"id": "0d1cbe5e-3de6-4f6a-9c53-bab32c168fbf",
				"name": "Condimentum aliquet",
				"price": 13.9,
				"quantity": 1
			}
		];
		expect(calcTotal(cartItems)).toBeCloseTo(348.32, 2);
	});

	it('should return the error object whose values are message, type, row, column as keys', () => {
		const expectedErrorObject = {
			type: 'cell',
			row: 4,
			column: 2,
			message: 'Expected cell to be a positive number but received "-10".'
		}

		expect(createError('cell', 4, 2, 'Expected cell to be a positive number but received "-10".'))
			.toEqual(expectedErrorObject);
	});

});

describe('CartParser - integration test', () => {
	it('should return correct cart object with JSON format', () => {
		const resultFileContent = JSON.parse(readFileSync(resultFilePath, 'utf-8', 'r'));
		resultFileContent.items.forEach(line => line.id = uuid());
		expect(parse(validFormatCsvFilePath)).toEqual(resultFileContent);
	});
});